import React, { Component } from "react";
import { Layout, Menu, Typography, Row, Col, Input, Button, message, Table, Empty, Space } from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    DownloadOutlined,
    UploadOutlined,
    HomeOutlined, SearchOutlined

} from "@ant-design/icons";

import axios from "axios";
import config from '../../config';
import { CSVLink } from "react-csv";
import Highlighter from "react-highlight-words";

const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;

class App extends Component {
    state = {
        course_name: "",
        course_code: "",
        courses: [],
        status: 0,
    };

    onChangeInputBox1 = (e) => {
        this.setState({ course_name: e.target.value });

    };
    onChangeInputBox2 = (e) => {
        this.setState({ course_code: e.target.value });
    };

    componentDidMount() {
        axios.post(config.serverurl + "/assignment_db/admin/courses")
            .then(res => {

                this.setState({ courses: res.data.courses });


            })
    }

    submit = () => {
        let flag = 0;

        if (this.state.course_name === "") {
            message.error("Enter Course Name");
            flag = 1;
            return false;
        }
        else if (this.state.course_code === "") {
            message.error("Enter Course Code");
            flag = 1;
            return false;
        }


        else if (flag === 0) {
            let data = {
                course_name: this.state.course_name,
                course_code: this.state.course_code,
            }
            axios.post(config.serverurl + "/assignment_db/admin/create_course", data)
                .then(res => {
                    if (res.data.status === 1) {
                        this.setState({ courses: res.data.courses });
                        message.success("Successfully Created");

                    }
                    else {
                        message.error("!Operation Failed");
                    }


                })
        }
    }

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };




    render() {
        const columns = [
            {
                title: 'Course Name',
                dataIndex: 'course_name',
                key: 'course_name',
                ...this.getColumnSearchProps('course_name'),
            },
            {
                title: 'Course Code',
                dataIndex: 'course_code',
                key: 'course_code',
                ...this.getColumnSearchProps('course_code'),
            }
        ]
        const headers = [
            { label: "Course Name", key: "course_name" },
            { label: "Course Code", key: "course_code" },
        ];

        return (
            <div>

                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>Create Course</Title>
                    </Col>
                    <Col span={6} >
                        <Input placeholder="Course Name" style={{ width: "100%" }} onChange={this.onChangeInputBox1} />

                    </Col>
                    <Col span={6}>
                        <Input placeholder="Course Code" style={{ width: "100%" }} onChange={this.onChangeInputBox2} />

                    </Col>
                    <Col span={6}>

                        <Button block type="primary" onClick={this.submit} style={{ width: "80%" }}>Create</Button>
                    </Col>

                    {
                        this.state.courses.length ?
                            <Col span={24}>
                                <CSVLink data={this.state.courses} headers={headers} filename={"Report Courses Statistics"}>
                                    <DownloadOutlined />Download Excel
                                </CSVLink>
                            </Col> :
                            ""}


                    {
                        this.state.courses.length ?

                            <Col span={18}>

                                <Table dataSource={this.state.courses} columns={columns} bordered />

                            </Col> :
                            <Col span={24} align="middle"><Empty /></Col>
                    }


                </Row>
            </div>
        );
    }
}

export default App;