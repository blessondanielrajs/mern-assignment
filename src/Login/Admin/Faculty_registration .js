import React, { Component } from "react";
import {
    Row, Col, Card, Typography, Button, Upload, message, Tag, Space, Statistic, Table, Input, Select
} from "antd";
import { UploadOutlined, DownloadOutlined, InboxOutlined } from '@ant-design/icons';
import { CSVLink } from "react-csv";
import axios from "axios";
import config from '../../config';
import Sample from '../../Documents/file.csv';
const { Title, Paragraph, Text } = Typography;
const { Dragger } = Upload;
const { Option } = Select;


class App extends Component {
    state = {
        status: 0,
        data: [],
        Name: ""
    };

    onChangeInputBox1 = (e) => {
        this.setState({ Name: e.target.value });

    };

    create = () => {

        let flag = 0;
        if (this.state.Name === "") {
            message.error("Invaild Input Name");
            flag = 1;
            return false;
        }
        else if (flag === 0) {
        let data = {
            name: this.state.Name
        }
        axios.post(config.serverurl + "/assignment_db/admin/single_faculty", data)
            .then(res => {
                if (res.data.status === 1) {

                    message.success("Successfully Created");

                }
                else {
                    message.error("!Operation Failed");
                }

            })
        }
    }



    onFileChange = async (info) => {
        const { status } = info.file;
        if (status !== 'uploading') {

        }
        if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
            this.setState({ data: info.file.response.addStatus });
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    };


    render() {




        const props = {
            accept: ".csv",
            name: 'file',
            multiple: true,
            action: config.serverurl + '/assignment_db/admin/bulk/faculty',
            onChange: this.onFileChange,
        };


        const columns = [
            {
                title: 'Type',
                dataIndex: 'json',
                key: 'key',

            },
            {
                title: 'Message',
                dataIndex: 'msg',
                key: 'key',
                render: (text, record) => <Tag color={record.addstatus === 0 ? "red" : "green"}>{text}</Tag>

            },


        ];


        const headers = [
            { label: "Type", key: "json" },
            { label: "Message", key: "msg" },

        ];


        return (
            <div>
                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>Faculty Registration</Title>
                    </Col>
                    <Col span={2}>
                        <Text level={3}>Single Registration *</Text>
                    </Col>
                    <Col span={6}>
                        <Input placeholder="Name" style={{ width: "100%" }} onChange={this.onChangeInputBox1} />
                    </Col>
                    <Col span={4}>

                        <Button type="primary" style={{ width: "100%" }} onClick={this.create}>Create</Button>

                    </Col>
                    <Col span={12}> </Col>

                    <Col span={2}>
                        <Text level={3}>Sample Template *</Text>
                    </Col>
                    <Col span={10}>

                        <Button target="_blank" href={Sample} type="primary">Download Template</Button>

                    </Col>
                    <Col span={12}></Col>

                    <Col span={2}>
                        <Text level={3}>Bulk Registration *</Text>
                    </Col>

                    <Col span={22}>
                        <Dragger{...props}>
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                band files
                            </p>
                        </Dragger>
                    </Col>

                    <Col span={24}>
                        <br />
                        <br />
                        {
                            this.state.data.length ?

                                <CSVLink data={this.state.data} headers={headers} filename={"Bulk Status"}>
                                    <DownloadOutlined />Download Excel
                                </CSVLink>
                                :
                                ""
                        }
                    </Col>

                    <Col span={12}>
                        {this.state.data.length > 0 ? <Table dataSource={this.state.data} columns={columns} /> : ""}
                    </Col>
                </Row>
            </div>
        );
    }
}


export default App;