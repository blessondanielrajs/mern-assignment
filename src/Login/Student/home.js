import React, { Component } from "react";
import {
    Layout,
    Menu,
    Typography,
    Row,
    Col,
    Table,
    Empty,
    Space,
    Button,
    Modal,
    message, Popconfirm, Tag, Input
} from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    UploadOutlined,
    HomeOutlined,
    SearchOutlined
} from "@ant-design/icons";
import config from "../../config";
import axios from "axios";
import Highlighter from "react-highlight-words";

import moment from "moment";
import momenttimezone from "moment-timezone";
import Editor from "./Editor";
momenttimezone.tz.setDefault("Asia/Kolkata");
const dateFormatList = "DD/MM/YYYY HH:mm:ss";

const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;
const date = parseInt(Date.now() / 1000);

class App extends Component {
    state = {
        collapsed: false,
        status: 0,
        data: [],
        question: "",
        Visible: "",
        Content: "",
        Record: []
    };

    componentDidMount() {
        let data = { _id: this.props.data._id };

        axios
            .post(config.serverurl + "/assignment_db/student/my_batches", data)
            .then((res) => {
                console.log(res.data)
                this.setState({ data: res.data.data });
            });
    }

    View = (record) => {
        let data = {
            batch_id: record.batch_id,
            student_id: this.props.data._id
        };
        console.log(data)
        axios
            .post(config.serverurl + "/assignment_db/student/question", data)
            .then((res) => {
                console.log(res.data)
                this.setState({ question: res.data.question });
            });

    };

    Content = (record) => {
        this.setState({ Content: record.content, Record: record, status: 0 });
       console.log(record)
    };



    confirm = (e) => {
        this.setState({ status: 1 });
    }

    cancel = (e) => {
        console.log(e);
    }

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    render() {
        const columns = [
            {
                title: "Batch Id",
                dataIndex: "batch_id",
                key: "_id",
            },

            {
                title: "Batch Name",
                dataIndex: "batch_name",
                key: "_id",
            },
            {
                title: "Faculty Id",
                dataIndex: "faculty_id",
                key: "_id",
            },
            {
                title: "Faculty Name",
                dataIndex: "faculty_name",
                key: "_id",
            },
            {
                title: "Course Name",
                dataIndex: "course_name",
                key: "_id",
            },
            {
                title: "Course Code",
                dataIndex: "course_code",
                key: "_id",
            },
            {
                title: "Questions",
                key: "_id",
                render: (text, record) => (
                    <Space>
                        <Button type="primary" onClick={this.View.bind(this, record)}>
                            VIEW
                        </Button>
                    </Space>
                ),
            },
            {
                title: "Question Count",
                dataIndex: "question_count",
                key: "question_count",
            },
            {
                title: "Submission Count",
                dataIndex: "submission_count",
                key: "submission_count",
            },
            {
                title: "Not Submission Count",
                dataIndex: "not_submission_count",
                key: "not_submission_count",
            },
            {
                title: "Accepted Count",
                dataIndex: "accepted_count",
                key: "accepted_count",
            },
            {
                title: "Resubmission Count",
                dataIndex: "rejected_count",
                key: "rejected_count",
            },
        ];


        const column = [
            {
                title: "Batch Name",
                dataIndex: "batch_name",
            },
            {
                title: "Course Name",
                dataIndex: "course_name",
            },
            {
                title: "Assignment Name",
                dataIndex: "assignment_name",
            },
            {
                title: "Open Date",
                render: (text, record) => (
                    <div>{moment.unix(record.open_date).format(dateFormatList)}</div>
                ),
            },
            {
                title: "Deadline Date",
                render: (text, record) => (
                    <div>{moment.unix(record.deadline_date).format(dateFormatList)}</div>
                ),
            },
            {
                title: "Date Now",
                render: (text, record) => (
                    <div>{moment.unix(date).format(dateFormatList)}</div>
                ),
            },
            {
                title: "Question",
                render: (text, record) => (
                    <Popconfirm
                        disabled={parseInt(record.open_date) >= date ? true : false || parseInt(record.deadline_date) <= date ? true : false}
                        title="Are you sure?"
                        onConfirm={this.confirm}
                        onCancel={this.cancel}
                        okText="Yes"
                        cancelText="No"
                    >

                        <Button type="primary" disabled={parseInt(record.open_date) >= parseInt(date) ? true : false || parseInt(record.deadline_date) <= parseInt(date) ? true : false}
                            onClick={this.Content.bind(this, record)}>
                            VIEW
                        </Button>
                    </Popconfirm>
                ),
            },
            {
                title: "Status",
                dataIndex: "status",
                render: (text, record) => (
                    <div>
                        {parseInt(text) === 3 ? <Tag color="blue">Resubmission</Tag>
                            : parseInt(text) === 1 ? <Tag color="orange">Pending</Tag>
                                : parseInt(text) === 2 ? <Tag color="green">Accept</Tag>
                                    : parseInt(text) === -1 ? <Tag color="red">Not Submit</Tag>
                                        : ""}
                    </div>
                ),

            },
            {
                title: "Mark",
                render: (text, record) => (
                    <Space>
                        {record.mark > 0 ? <Tag color="green">{record.mark}</Tag> :
                            <Tag color="red">{record.mark}</Tag>}
                    </Space>
                ),

            },
            {
                title: "Message",
                render: (text, record) => (
                    <Space>
                        {record.msg === "empty" ? "Empty"

                            : record.msg === "No" ? "Empty"
                                : record.msg}
                    </Space>
                ),

            },
        ];

        return (
            <div>
                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>My Batches</Title>
                    </Col>

                    {this.state.data.length ? (
                        <Col span={24}>
                            <Table dataSource={this.state.data} columns={columns} bordered />
                        </Col>
                    ) : (
                        <Col span={24} align="middle">
                            <Empty />
                        </Col>
                    )}

                    {this.state.question.length ? (
                        <Col span={24}>
                            <Table
                                dataSource={this.state.question}
                                columns={column}
                                bordered
                            />
                        </Col>
                    ) : (
                        ""
                    )}
                </Row>

                {this.state.status === 1 ? <Editor Content={this.state.Content} Record={this.state.Record} Name={this.props.data.student_name} Id={this.props.data._id} /> : ""}
            </div>
        );
    }
}

export default App;
