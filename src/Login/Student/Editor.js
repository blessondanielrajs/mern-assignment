import React, { Component } from "react";
import { Layout, Menu, Typography, Modal, Empty, Row, Col, Descriptions, Button, message } from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,

    UploadOutlined,
    HomeOutlined

} from "@ant-design/icons";
import axios from "axios";
import config from '../../config';
import { CKEditor } from 'ckeditor4-react';
import moment from "moment";
import momenttimezone from "moment-timezone";
momenttimezone.tz.setDefault("Asia/Kolkata");
const dateFormatList = "DD/MM/YYYY HH:mm:ss";
const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;

class App extends Component {
    state = {
        collapsed: false,
        status: 0,
        Answers: "",
        Batch_id: ""
    }

    componentDidMount() {
        let data = {
            batch_name: this.props.Record.batch_name
        }
        axios.post(config.serverurl + "/assignment_db/student/batch_id", data)
            .then(res => {
                this.setState({ Batch_id: res.data.batch_id })

            })

    }

    onEditorChange = (evt) => {
        this.setState({
            Answers: evt.editor.getData()

        });
    }

    submit = () => {
        let data = {
            _id: this.props.Id,
            student_name: this.props.Name,
            batch_id: this.state.Batch_id,
            batch_name: this.props.Record.batch_name,
            question_id: this.props.Record._id,
            course_code: this.props.Record.course_code,
            course_name: this.props.Record.course_name,
            question_name: this.props.Record.assignment_name,
            faculty_id: this.props.Record.faculty_id,
            answer: this.state.Answers,
        }
        axios.post(config.serverurl + "/assignment_db/student/submission", data)
            .then(res => {
                if (res.data.status === 1) {
                    message.success("Successfully submited");

                }
                else {
                    message.error("!Operation Failed");
                }

            })
    }

    render() {
        console.log(this.state.Batch_id)
        return (
            <div>


                <Row gutter={[16, 24]}>
                    <Col span={24} >
                        <Descriptions
                            title="Question"
                            bordered
                            size="small"
                            column={{ xxl: 2, xl: 2, lg: 2, md: 1, sm: 1, xs: 1 }}
                        >
                            <Descriptions.Item label="Batch Name">{this.props.Record.batch_name}</Descriptions.Item>
                            <Descriptions.Item label="Course Name">{this.props.Record.course_name}</Descriptions.Item>
                            <Descriptions.Item label="Course Code">{this.props.Record.course_code}</Descriptions.Item>
                            <Descriptions.Item label="Assignment name">{this.props.Record.assignment_name}</Descriptions.Item>
                            <Descriptions.Item label="Open Date">{moment.unix(this.props.Record.open_date).format(dateFormatList)}</Descriptions.Item>
                            <Descriptions.Item label="Deadline Date">{moment.unix(this.props.Record.deadline_date).format(dateFormatList)}</Descriptions.Item>
                            <Descriptions.Item label="Question" span={2}> <CKEditor
                                initData={this.props.Content}
                                readOnly={true}
                            /></Descriptions.Item>
                            <Descriptions.Item label="Answers" span={2}>
                                <CKEditor

                                    onChange={this.onEditorChange}

                                />
                            </Descriptions.Item>
                        </Descriptions>


                        <Col span={24} offset={20} >
                            <Button block type="primary" onClick={this.submit} style={{ width: "100%" }}>Submit</Button>

                        </Col>



                    </Col>
                </Row>



            </div>
        );
    }
}

export default App;