import React, { Component } from "react";
import { Layout, Space, Typography, Row, Col, Input, Button, Select, message, Table, Empty, Modal, Upload, Tag, Spin } from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    InboxOutlined,
    UploadOutlined,
    HomeOutlined,
    DownloadOutlined,
    SearchOutlined

} from "@ant-design/icons";
import Sample from '../../Documents/file3.csv';
import { CSVLink } from "react-csv";
import Highlighter from "react-highlight-words";

import axios from "axios";
import config from '../../config';
const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;
const { Option } = Select;
const { Dragger } = Upload;

class App extends Component {
    state = {
        Course_name: [],
        CourseName: "",
        CourseCode: "",
        Batch_name: "",
        Batch_data: [],
        status: 0,
        setIsModalVisible: "",
        Batch_id: "",
        Batch_course: "",
        data: [],
        view: [],
        Visible: ""

    }

    componentDidMount() {
        let data = {
            faculty_id: this.props.data._id,
        }
        axios.post(config.serverurl + "/assignment_db/faculty/course_name", data)
            .then(res => {

                this.setState({ Course_name: res.data.Course_name, Batch_data: res.data.Batch_data });
                console.log(res.data.batch_data)

            })
    }

    onChangeInputBox1 = (e) => {
        this.setState({ Batch_name: e.target.value });

    };

    handleChangeCourseName = (value) => {
        this.setState({ CourseName: value })
        let data = {
            course_code: value
        }
        axios.post(config.serverurl + "/assignment_db/faculty/course_code", data)
            .then(res => {
                this.setState({ CourseCode: res.data.Course_code });
            })

    }




    create = () => {
        let flag = 0;
        if (this.state.CourseName === "") {
            message.error("Select Course");
            flag = 1;
            return false;
        }
        else if (this.state.Batch_name === "") {
            message.error("Enter Batch Name");
            flag = 1;
            return false;
        }

        else if (flag === 0) {

            let data = {

                batch_name: this.state.Batch_name,
                faculty_id: this.props.data._id,
                faculty_name: this.props.data.faculty_name,
                course_name: this.state.CourseName,
                course_code: this.state.CourseCode,

            }

            axios.post(config.serverurl + "/assignment_db/faculty/create_batch", data)
                .then(res => {
                    if (res.data.status === 1) {
                        this.setState({ Batch_data: res.data.Batch_data });
                        message.success("Successfully Created");

                    }
                    else {
                        message.error("!Operation Failed");
                    }

                })
        }
    }

    showModal = (data) => {
        this.setState({ setIsModalVisible: true });
        this.setState({ Batch_id: data._id, Batch_course: data.course_code });

    }
    showModaldata = async (record) => {
        this.setState({ Batch_id: record._id, Batch_course: record.course_code });
        let data = {
            Batch_course: record.course_code
        }
        await axios.post(config.serverurl + "/assignment_db/faculty/batch_data", data)
            .then(res => {
                 this.setState({ view: res.data.data, Visible: true })
            })
    }

    handleOk = () => {
        this.setState({ setIsModalVisible: false });
    };

    handleCancel = () => {
        this.setState({ setIsModalVisible: false });
    };


    handleOk1 = () => {
        this.setState({ Visible: false });
    };

    handleCancel1 = () => {
        this.setState({ Visible: false });
    };

    onFileChange = async (info) => {
        const { status } = info.file;
        if (status !== 'uploading') {

        }
        if (status === 'done') {
            message.success(`${info.file.name} file Asigned successfully.`);
            this.setState({ data: info.file.response.addstatus });

        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    };


    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    render() {
        let data = {
            batch_id: this.state.Batch_id,
            course_code: this.state.Batch_course
        }
        const props = {
            accept: ".csv",
            name: 'file',
            multiple: true,
            action: config.serverurl + "/assignment_db/faculty/bulk", data,
            onChange: this.onFileChange,
        };

        const columns = [
            {
                title: 'Batch Id',
                dataIndex: '_id',
                key: '_id',
                sorter: (a, b) => a._id - b._id,

            },
            {
                title: 'Batch Name',
                dataIndex: 'batch_name',
                key: 'batch_name',
                ...this.getColumnSearchProps('batch_name'),

            },
            {
                title: 'Course Code',
                dataIndex: 'course_code',
                key: 'course_code',
                ...this.getColumnSearchProps('course_code'),            },
            {
                title: 'Course Name',
                dataIndex: 'course_name',
                key: 'course_name',
                ...this.getColumnSearchProps('course_name'),            },
            {
                title: 'Action',
                key: '_id',
                render: (text, record) => (

                    <Space>
                        <Button type="primary" onClick={this.showModaldata.bind(this, record)}>VIEW</Button>
                        <Button type="primary" onClick={this.showModal.bind(this, record)}>ASSIGN</Button>

                    </Space>

                )
            }
        ];
        const headers = [
            { label: "Type", key: "json" },
            { label: "Message", key: "msg" },

        ];


        const column = [
            {
                title: 'Type',
                dataIndex: 'json',
                key: 'json',
                ...this.getColumnSearchProps('json'),

            },
            {
                title: 'Message',
                dataIndex: 'msg',
                key: 'msg',
                ...this.getColumnSearchProps('msg'),
                render: (text, record) => <Tag color={record.addstatus === 0 ? "red" : "green"}>{text}</Tag>
                

            },


        ];

        const view = [
            {
                title: 'Student Name',
                dataIndex: 'student_name',
                key: 'student_name',
                ...this.getColumnSearchProps('student_name'),
                
            },
            {
                title: 'Department',
                dataIndex: 'department',
                key: 'department',
                ...this.getColumnSearchProps('department'),

            },
            {
                title: 'Batch Id',
                key: 'key',
                render: (text, record) => (
                    <div>
                        {this.state.Batch_id}
                    </div>
                )
            },
            {
                title: 'Course Code',
                key: 'key',
                render: (text, record) => (
                    <div>
                        {this.state.Batch_course}
                    </div>
                )
            }
        ]

     

        return (
            <div>
                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>Create Batch</Title>
                    </Col>
                    <Col span={6} >
                        <Select defaultValue="" style={{ width: "100%" }} onChange={this.handleChangeCourseName}>
                            <Option disabled value="">Choose One</Option>
                            {
                                this.state.Course_name.length > 0 ?
                                    this.state.Course_name.map(function (item, i) {
                                        return <Option key={i} value={item}>{item}</Option>
                                    })
                                    : ''
                            }
                        </Select>

                    </Col>


                    <Col span={6}>
                        <Input placeholder="Batch Name" style={{ width: "100%" }} onChange={this.onChangeInputBox1} />

                    </Col>
                    <Col span={6}>

                        <Button block type="primary" onClick={this.create} style={{ width: "80%" }}>Create</Button>
                    </Col>
                    {
                        this.state.Batch_data.length ?

                            <Col span={18}>

                                <Table dataSource={this.state.Batch_data} columns={columns} bordered />

                            </Col> :
                            <Col span={24} align="middle"><Empty /></Col>
                    }



                </Row>
                <Modal title="Assign Student" visible={this.state.setIsModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>

                    <Row gutter={[16, 16]}>
                        <Col span={6}>
                            <Text level={3}>Sample Template *</Text>
                        </Col>
                        <Col span={10}>
                            <Button target="_blank" href={Sample} type="primary">Download Template</Button>
                        </Col>
                    </Row>
                    <br />
                    <Row gutter={[16, 16]} >
                        <Col span={6}>
                            <Text level={3}>Bulk Registration *</Text>
                        </Col>
                        <Col span={10}>
                            <Dragger{...props}>
                                <p className="ant-upload-drag-icon">
                                    <InboxOutlined />
                                </p>
                                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                <p className="ant-upload-hint">
                                    Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                    band files
                                </p>
                            </Dragger>
                        </Col>
                        <Col span={24}>
                            <br />
                            <br />


                        </Col>


                        <Col span={24}>
                            <br />
                            <br />
                            {
                                this.state.data.length ?
                                    <div>
                                        <CSVLink data={this.state.data} headers={headers} filename={"Bulk Status"}>
                                            <DownloadOutlined />Download Excel
                                        </CSVLink>
                                        <Table dataSource={this.state.data} columns={column} />
                                    </div>
                                    : ""}
                        </Col>


                    </Row>

                </Modal>

                <Modal title="My Batches" visible={this.state.Visible} onOk={this.handleOk1} onCancel={this.handleCancel1}>
                   
                
                    {
                        this.state.view.length ? <div> <Table dataSource={this.state.view} columns={view} /></div> : <Empty />}


                </Modal>

            </div>
        );
    }
}

export default App;