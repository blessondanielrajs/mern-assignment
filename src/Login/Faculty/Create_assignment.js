import React, { Component } from "react";
import { Layout, Menu, Typography, Row, Col, Input, Button, message, Select, DatePicker, Space } from "antd";
import axios from "axios";
import config from '../../config';
import { CKEditor } from 'ckeditor4-react';
import moment from 'moment';
import momenttimezone from 'moment-timezone';
momenttimezone.tz.setDefault("Asia/Kolkata");
const dateFormatList = 'DD/MM/YYYY HH:mm:ss';
const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;
const { Option } = Select;


class Create_assignment extends Component {
    state = {
        status: 0,
        Course_name: [],
        CourseName: "",
        CourseCode: "",
        Assignment_Name: "",
        Content: "",
        OpenDate: "",
        DeadLineDate: "",
        Batch_data: "",
        Batch_Name: "",
        Batch_id: ""

    };

    componentDidMount() {
        let data = { "faculty_id": this.props.data._id }
        axios.post(config.serverurl + "/assignment_db/faculty/course_name", data)
            .then(res => {

                this.setState({ Course_name: res.data.Course_name });
                this.setState({ Batch_data: res.data.batch_detail });
                console.log(res.data.batch_detail)

            })
    }

    handleChangeCourseName = (value) => {
        this.setState({ CourseName: value })
        let data = {
            course_code: value
        }
        axios.post(config.serverurl + "/assignment_db/faculty/course_code", data)
            .then(res => {
                this.setState({ CourseCode: res.data.Course_code });
            })

    }


    handleChangeBatchName = (value) => {
        this.setState({ Batch_Name: value });
        let data = {
            batch_name: value
        }
        axios.post(config.serverurl + "/assignment_db/faculty/batch_id", data)
            .then(res => {
                this.setState({ Batch_id: res.data.Batch_id });
            })


    }


    onChangeInputBox1 = (e) => {
        this.setState({ Assignment_Name: e.target.value });

    };
    onOk1 = (value) => {
        var i = (moment(value).unix());
        this.setState({ OpenDate: i })
        // console.log(moment.unix(i).format(dateFormatList))
    }

    onOk2 = (value) => {
        var i = (moment(value).unix());
        this.setState({ DeadLineDate: i })
        // console.log(moment.unix(i).format(dateFormatList))
    }


    onEditorChange = (evt) => {
        this.setState({
            Content: evt.editor.getData()
        });
    }


    create = () => {
        let flag = 0;
        if (this.state.CourseName === "") {
            message.error("Select Course");
            flag = 1;
            return false;
        }
        else if (this.state.Assignment_Name === "") {
            message.error("Enter Assignment Name");
            flag = 1;
            return false;
        }
        else if (this.state.OpenDate === "") {
            message.error("Enter Open Date");
            flag = 1;
            return false;
        }
        else if (this.state.DeadLineDate === "") {
            message.error("Enter Deadline Date");
            flag = 1;
            return false;
        }
        else if (this.state.Content === "") {
            message.error("Enter Content");
            flag = 1;
            return false;
        }
        else if (this.state.OpenDate > this.state.DeadLineDate) {
            message.error("Enter Correct Date");
            flag = 1;
            return false;
        }
        else if (flag === 0) {

            let data = {
                faculty_id: this.props.data._id,
                faculty_name: this.props.data.faculty_name,
                batch_name: this.state.Batch_Name,
                batch_id: this.state.Batch_id,
                course_name: this.state.CourseName,
                course_code: this.state.CourseCode,
                assignment_name: this.state.Assignment_Name.toUpperCase(),
                open_date: this.state.OpenDate,
                deadline_date: this.state.DeadLineDate,
                content: this.state.Content,
            }

            axios.post(config.serverurl + "/assignment_db/faculty/create_assignment", data)
                .then(res => {
                    if (res.data.status === 1) {
                        message.success("Successfully Created");

                    }
                    else {
                        message.error("!Operation Failed");
                    }

                })
        }
    }


    render() {

        return (
            <div>


                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>Create Assignment</Title>
                    </Col>
                    <Col span={6} >
                        <Select defaultValue="" style={{ width: "100%" }} onChange={this.handleChangeBatchName}>
                            <Option disabled value="">Choose One</Option>
                            {
                                this.state.Batch_data.length > 0 ?
                                    this.state.Batch_data.map(function (item, i) {
                                        return <Option key={i} value={item}>{item}</Option>
                                    })
                                    : ''
                            }
                        </Select>

                    </Col>


                    <Col span={6} >
                        <Select defaultValue="" style={{ width: "100%" }} onChange={this.handleChangeCourseName}>
                            <Option disabled value="">Choose One</Option>
                            {
                                this.state.Course_name.length > 0 ?
                                    this.state.Course_name.map(function (item, i) {
                                        return <Option key={i} value={item}>{item}</Option>
                                    })
                                    : ''
                            }
                        </Select>

                    </Col>
                    <Col span={6} >
                        <Input placeholder="Assignment Name" style={{ width: "100%" }} onChange={this.onChangeInputBox1} />
                    </Col>

                    <Col span={12} >

                        <DatePicker placeholder="Open Date" showTime onOk={this.onOk1} />
                        <DatePicker placeholder="Deadline Date" showTime onOk={this.onOk2} />

                    </Col>

                    <Col span={24} >
                        <CKEditor
                            initData={<p>Question Description Type Here</p>}
                            onInstanceReady={() => {
                                message.success('Editor is ready!');
                            }}

                            onChange={this.onEditorChange}

                        />
                    </Col>
                    <Col span={24} offset={21} >
                        <Button block type="primary" onClick={this.create} style={{ width: "10%" }}>Create</Button>

                    </Col>

                </Row>

            </div>
        );
    }
}

export default Create_assignment;