import React, { Component } from "react";
import { Layout, Menu, Typography, Table, Button, Space, Row, Col, Modal, Descriptions, message, Popover, InputNumber, Input } from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    SearchOutlined,
    UploadOutlined,
    HomeOutlined

} from "@ant-design/icons";
import Highlighter from "react-highlight-words";

import config from "../../config";
import { CKEditor } from 'ckeditor4-react';
import axios from "axios";
import moment from "moment";
import momenttimezone from "moment-timezone";
momenttimezone.tz.setDefault("Asia/Kolkata");
const dateFormatList = "DD/MM/YYYY HH:mm:ss";
const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;
const { TextArea } = Input;
class App extends Component {
    state = {
        status: 0,
        details: [],
        setIsModalVisible: "",
        question: "",
        answer: "",
        visible1: false,
        visible2: false,
        _id: "",
        Mark: "",
        Message: ""

    }

    componentDidMount() {
        let data = {
            _id: this.props.data._id
        }
        console.log(data)
        axios.post(config.serverurl + "/assignment_db/faculty/submission_details", data)
            .then((res) => {
                this.setState({ details: res.data.details });
            });
    }

    showModal = (record) => {
        this.setState({ _id: record._id })
        let data = {
            question_id: record.question_id
        }
        axios.post(config.serverurl + "/assignment_db/faculty/question_view", data)
            .then((res) => {
                this.setState({ question: res.data.question, answer: record.answer, setIsModalVisible: true });
            });

    }

    Accept = () => {

        let data = {
            _id: this.state._id,
            mark: this.state.Mark
        }
        axios.post(config.serverurl + "/assignment_db/faculty/accept", data)
            .then((res) => {
                if (res.data.status === 1) {
                    message.success("sucessfully Accepted");
                }
                else {
                    message.error("Operation Failed")
                }
                this.setState({ visible1: false });
            });
    }

    Reject = () => {
        let data = {
            _id: this.state._id,
            msg: this.state.Message
        }
        axios.post(config.serverurl + "/assignment_db/faculty/reject", data)
            .then((res) => {
                if (res.data.status === 1) {
                    message.success("sucessfully Rejected");
                }
                else {
                    message.error("Operation Failed")
                }
                this.setState({ visible2: false });
            });
    }


    handleOk = () => {
        this.setState({ setIsModalVisible: false });
    };

    handleCancel = () => {
        this.setState({ setIsModalVisible: false });
    };

    hide = () => {
        this.setState({
            visible: false,
        });
    };
    Accept_Visible = () => {
        this.setState({ visible1: true });

    };
    Reject_Visible = () => {
        this.setState({ visible2: true });
    };

    Mark = (value) => {
        this.setState({ Mark: value });
    }
    Message = (e) => {
        this.setState({ Message: e.target.value });
    };

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    render() {
        const columns = [
            {
                title: "Batch Name",
                dataIndex: "batch_name",
                key: "batch_name",
                ...this.getColumnSearchProps('batch_name'),
            },

            {
                title: "Course Code",
                dataIndex: "course_code",
                key: "course_code",
                ...this.getColumnSearchProps('course_code'),

            },
            {
                title: "Course Name",
                dataIndex: "course_name",
                key: "course_name",
                ...this.getColumnSearchProps('course_name'),

            },
            {
                title: "Question Name",
                dataIndex: "question_name",
                key: "question_name",
                ...this.getColumnSearchProps('question_name'),

            },
            {
                title: "Student Name",
                dataIndex: "student_name",
                key: "student_name",
                ...this.getColumnSearchProps('student_name'),

            },
            {
                title: "Submission Date",
                render: (text, record) => (
                    <div>{moment.unix(record.submission_date).format(dateFormatList)}</div>
                ),
            },
            {
                title: "View",
                render: (text, record) => (
                    <Space>
                        <Button type="primary" onClick={this.showModal.bind(this, record)}>VIEW</Button>
                    </Space>
                ),
            },

        ];
        console.log(this.state.question)
        return (
            <div>
                <Row gutter={[16, 24]}>
                    {this.state.details ? (
                        <Col span={24}>
                            <Table
                                dataSource={this.state.details}
                                columns={columns}
                                bordered
                            />
                        </Col>
                    ) : (
                        ""
                    )}
                </Row>
                <Modal title="Question Ans Answer" width={"100%"} visible={this.state.setIsModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>


                    <Row gutter={[16, 24]}>
                        <Col span={24} >
                            <Descriptions
                                title="Question"
                                bordered
                                size="small"
                                column={{ xxl: 2, xl: 2, lg: 2, md: 1, sm: 1, xs: 1 }}
                            >

                                <Descriptions.Item label="Question" span={2}>
                                    <CKEditor
                                        initData={this.state.question}
                                        readOnly={true}
                                    /></Descriptions.Item>
                                <Descriptions.Item label="Answers" span={2}>
                                    <CKEditor

                                        initData={this.state.answer}
                                        readOnly={true}

                                    />
                                </Descriptions.Item>
                            </Descriptions>
                        </Col>
                    </Row>
                    <Row gutter={[16, 24]}>
                        <Col span={12} >
                            <Popover
                                content={<Row gutter={[16, 24]}>
                                    <Col span={12} >
                                        <InputNumber min={1} max={10} onChange={this.Mark} />
                                    </Col>
                                    <Col span={12} >
                                        <Button type="primary" onClick={this.Accept}>Submit</Button>
                                    </Col>
                                </Row>
                                }
                                title="Mark"
                                trigger="click"
                                visible={this.state.visible1}
                                onVisibleChange={this.Accept_Visible}
                            >
                                <Col span={2}>
                                    <Button type="primary">Accept</Button>
                                </Col>
                            </Popover>
                        </Col>
                        <Col span={12} >
                            <Popover
                                content={<Row gutter={[16, 24]}>
                                    <Col span={12} >
                                        <TextArea rows={4} onChange={this.Message} />
                                    </Col>
                                    <Col span={12} >
                                        <Button type="primary" onClick={this.Reject}>Submit</Button>
                                    </Col>
                                </Row>
                                }
                                title="Mark"
                                trigger="click"
                                visible={this.state.visible2}
                                onVisibleChange={this.Reject_Visible}
                            >
                                <Col span={2}>
                                    <Button type="danger" >Reject</Button>
                                </Col>
                            </Popover>
                        </Col>
                    </Row>
                </Modal>

            </div>
        );
    }
}

export default App;