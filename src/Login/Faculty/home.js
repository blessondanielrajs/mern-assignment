import React, { Component } from "react";
import { Layout, Menu, Typography, Row, Col, Table, Empty, Button, Input, Space } from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    DownloadOutlined,
    UploadOutlined,
    HomeOutlined, SearchOutlined

} from "@ant-design/icons";

import config from "../../config";
import axios from "axios";
import { CSVLink } from "react-csv";
import Highlighter from "react-highlight-words";

const { Header, Sider, Content } = Layout;
const { Title, Paragraph, Text, Link } = Typography;

class App extends Component {
    state = {
        collapsed: false,
        status: 0,
        Batch_data: []
    }

    componentDidMount() {
        let data = {
            faculty_id: this.props.data._id,
        }
        axios.post(config.serverurl + "/assignment_db/faculty/dashboard_count", data)
            .then(res => {
                this.setState({ Batch_data: res.data.Batch_data });

            })
    }
    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };
    render() {
        const columns = [
            {
                title: 'Batch Id',
                dataIndex: 'batch_id',
                key: 'batch_id',
                sorter: (a, b) => a.batch_id - b.batch_id,

            },
            {
                title: 'Batch Name',
                dataIndex: 'batch_name',
                key: 'batch_name',
                ...this.getColumnSearchProps('batch_name'),

            },
            {
                title: 'Course Code',
                dataIndex: 'course_code',
                key: 'course_code',
                ...this.getColumnSearchProps('course_code'),
            },
            {
                title: 'Course Name',
                dataIndex: 'course_name',
                key: 'course_name',
                ...this.getColumnSearchProps('course_name'),
            },
             {
                title: "Question Count",
                dataIndex: "question_count",
                key: "question_count",
                 sorter: (a, b) => a.question_count - b.question_count,
            },
            {
                title: "Not Submission Count",
                dataIndex: "not_submission_count",
                key: "not_submission_count",
                sorter: (a, b) => a.not_submission_count - b.not_submission_count,
            },
            {
                title: 'Submission Count',
                dataIndex: 'submission_count',
                key: 'submission_count',
                sorter: (a, b) => a.submission_count - b.submission_count,
            },

            {
                title: 'Accepted Count',
                dataIndex: 'accepted_count',
                key: 'accepted_count',
                sorter: (a, b) => a.accepted_count - b.accepted_count,
            },
            {
                title: 'Rejected Count',
                dataIndex: 'rejected_count',
                key: 'rejected_count',
                sorter: (a, b) => a.rejected_count - b.rejected_count,

            },
           


        ];

        const headers = [
            { label: "Batch Id", key: "batch_id" },
            { label: "Batch Name", key: "batch_name" },
            { label: "Course Code", key: "course_code" },
            { label: "Course Name", key: "course_name" },
            { label: "Submission Count", key: "submission_count" },
            { label: "Accepted Count", key: "accepted_count" },
            { label: "Rejected Count", key: "rejected_count" },

        ];
        return (
            <div>
                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>Dashboard</Title>
                    </Col>

                    {
                        this.state.Batch_data.length ?
                            <Col span={24}>
                                <CSVLink data={this.state.Batch_data} headers={headers} filename={"Report Statistics"}>
                                    <DownloadOutlined />Download Excel
                                </CSVLink>
                            </Col> :
                            ""}

                    {
                        this.state.Batch_data.length ?

                            <Col span={24}>

                                <Table dataSource={this.state.Batch_data} columns={columns} bordered />

                            </Col> :
                            <Col span={24} align="middle"><Empty /></Col>
                    }
                </Row>
            </div>
        );
    }
}

export default App;