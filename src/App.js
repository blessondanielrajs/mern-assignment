import React, { Component } from "react";
import {
  Card, Col, Row, Input, Button, PageHeader, message, Popconfirm
} from "antd";
import { LogoutOutlined } from '@ant-design/icons';
import "./App.less";
import Faculty from './Login/Faculty';
import Admin from './Login/Admin';
import Student from './Login/Student';
import axios from "axios";
import config from './config';


class App extends Component {
  state = {
    USERNAME: "",
    PASSWORD: "",
    key: 0,
    userDetails: {},
    status: 0
  };


  onChangeInputBox1 = (e) => {
    this.setState({ USERNAME: e.target.value });

  };
  onChangeInputBox2 = (e) => {
    this.setState({ PASSWORD: e.target.value });

  };



  login = () => {
    let flag = 0;

    let PASSWORD = this.state.PASSWORD;

    var pattern_password = /^[A-Za-z]\w{4,14}$/;
    if (this.state.USERNAME === "") {
      message.error("Invaild Input Username");
      flag = 1;
      return false;
    }
    else if (!PASSWORD === "" || !pattern_password.test(this.state.PASSWORD)) {
      message.error("Invaild Password");
      flag = 1;
      return false;
    }
    else if (flag === 0) {

      let data = {
        USERNAME: this.state.USERNAME.trim(),
        PASSWORD: this.state.PASSWORD.trim(),
      }


      axios.post(config.serverurl + "/assignment_db/login", data)
        .then(res => {

          if (res.data.Status === 1) {

            this.setState({ userDetails: res.data.user_details });
            if (res.data.user_details.role === 'faculty') {
              this.setState({ status: 1 });
            }
            else if (res.data.user_details.role === 'admin') {
              this.setState({ status: 2 });
            }
            else if (res.data.user_details.role === 'student') {
              this.setState({ status: 3 });

            }

          }
          else {
            message.error("Invalid User !");
          }
        })
    }
  }

  logout = () => {
    this.setState({
      status: 0, USERNAME: "",
      PASSWORD: "",
    });
  }

  render() {


    return (
      <div>
        <PageHeader
          className="site-page-header"
          title="Assignment"
          extra={[
            <Button className={this.state.status === 0 ? "hide" : " "} type="primary" key="1" onClick={this.logout} danger><LogoutOutlined />Logout</Button>,
          ]}
        />

        {this.state.status === 1 ? <Faculty data={this.state.userDetails} />
          : this.state.status === 2 ? (<Admin data={this.state.userDetails} />)
            : this.state.status === 3 ? (<Student data={this.state.userDetails} />)
              : (
                <div>
                  <Row justify="center">
                    <Col span={6} >
                      <Card title="Login Portal" style={{ width: "100%", marginTop: "40%" }} bordered type="inner">
                        <Row gutter={[16, 16]} align="middle">
                          <Col span={24}><Input placeholder="Username" onChange={this.onChangeInputBox1} /></Col>
                          <Col span={24}><Input placeholder="Password" onChange={this.onChangeInputBox2} /></Col>
                          <Col span={24}><Button block type="primary" onClick={this.login}>Submit</Button></Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>

                </div>
              )}
      </div>
    );
  }
}


export default App;

